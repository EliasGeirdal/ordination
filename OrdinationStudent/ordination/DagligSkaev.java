package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

public class DagligSkaev extends Ordination {
	private List<Dosis> dosis = new LinkedList<>();

	public DagligSkaev(Patient patient, LocalDate startDen, LocalDate slutDen) {
		super(patient, startDen, slutDen);
		// TODO Auto-generated constructor stub
	}

	// TODO
	public void opretDosis(LocalTime tid, double antal) {
		// TODO
		Dosis dosis = new Dosis(tid, antal);
		if (!this.dosis.contains(dosis))
			this.dosis.add(dosis);
	}

	@Override
	public double samletDosis() {
		// TODO Auto-generated method stub
		return dosis.size() * Double.parseDouble(super.getLaemiddel().getEnhed());
	}

	@Override
	public double doegnDosis() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "Daglig skaev";
	}

	public List<Dosis> getDosis() {
		return new LinkedList<Dosis>(dosis);
	}

}
