package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

public class PN extends Ordination {

	private double antalEnheder;
	private List<LocalDate> alleDosis = new LinkedList<LocalDate>();
	private LocalDate foersteDag;

	public PN(Patient patient, LocalDate startDen, LocalDate slutDen, double antalEnheder) {
		super(patient, startDen, slutDen);
		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		// TODO
		boolean gyldig = false;
		if (givesDen.isAfter(super.getStartDen()) && givesDen.isBefore(super.getSlutDen())) {
			gyldig = true;
			alleDosis.add(givesDen);
			if (alleDosis.size() == 1)
				foersteDag = givesDen;
		}
		return gyldig;
	}

	@Override
	public int antalDage() {
		return (int) ChronoUnit.DAYS.between(foersteDag, super.getSlutDen()) + 1;
	}

	@Override
	public double doegnDosis() {
		// TODO
		return (alleDosis.size() * antalEnheder) / antalDage();
	}

	@Override
	public double samletDosis() {
		// TODO
		return alleDosis.size() * antalEnheder;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		// TODO
		return alleDosis.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "PN";
	}

}
